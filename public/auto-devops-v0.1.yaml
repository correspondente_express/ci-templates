include:
    - 'https://correspondente_express.gitlab.io/ci-templates/definitions/default-definitions.gitlab-ci.yaml'
    - 'https://correspondente_express.gitlab.io/ci-templates/definitions/default-refs-definitions.gitlab-ci.yaml'
    - 'https://correspondente_express.gitlab.io/ci-templates/jobs/job-build-java.gitlab-ci.yaml'
    - 'https://correspondente_express.gitlab.io/ci-templates/jobs/job-build-node.gitlab-ci.yaml'
    - 'https://correspondente_express.gitlab.io/ci-templates/jobs/job-build-nodejs.gitlab-ci.yaml'
    - 'https://correspondente_express.gitlab.io/ci-templates/jobs/job-build-image.gitlab-ci.yaml'

#####################################
#  DEPLOY KUBERNETES JOBS
#####################################

# -----------------------------------
#  TEMPLATES
# -----------------------------------

.configure_kubectl: &configure_kubectl
    - SERVER=${HELM_KUBERNETES_SERVER}
    - if [ -z "$HELM_KUBERNETES_SERVER" ]; then SERVER="https://kubernetes.default.svc.cluster.local"; fi
    - kubectl config set-cluster kubernetes --server="$SERVER" --insecure-skip-tls-verify=true
    - kubectl config set-credentials helm-deployer --token="$HELM_KUBERNETES_TOKEN"
    - kubectl config set-context kubernetes --cluster="kubernetes" --namespace="$HELM_KUBERNETES_NAMESPACE" --user="helm-deployer"
    - kubectl config use-context kubernetes
    - echo "Server $SERVER configured"

.deploy-kubernetes: &deploy-kubernetes-template
    image: $HELM_DEPLOY_IMAGE
    when: manual
    allow_failure: true
    except:
        refs:
            - schedules
        variables:
            - $KUBERNETES_ENABLED == "false"
    before_script:  
        - *configure_kubectl
    script: |
        bash
        if [ -z "$APPNAME" ]; then
            echo "A variavel APPNAME nao foi definida"
        	exit 1
        fi

        echo "Publicando no namespace "
        echo "$HELM_KUBERNETES_NAMESPACE"

        if [ -z "$HELM_KUBERNETES_NAMESPACE" ]; then
            echo "A variavel HELM_KUBERNETES_NAMESPACE nao foi definida"
        	exit 1
        fi

        export STAGING_URL=${APPNAME}-${CI_COMMIT_REF_SLUG}.${KUBERNETES_DOMAIN}
        mkdir -p /tmp/generated/configs
        envsubst <${CI_PROJECT_DIR}/configs/kubernetes/${ENV}.yaml > /tmp/generated/configs/${ENV}.yaml
        
        if [ -z "$ENABLE_CE_REPOS" ]; then
            ENABLE_CE_REPOS="false"
        fi	

        if [ "$ENABLE_CE_REPOS" = "true" ]; then
            helm repo add ce $HELM_CE_REPOSITORY
        fi
        
        helm-deployer repo update

        helm-deployer list -n $HELM_KUBERNETES_NAMESPACE -f $APPNAME-$CI_COMMIT_REF_SLUG -o json 
        
        echo "Publicando no namespace $HELM_KUBERNETES_NAMESPACE"
        CHART_INSTALED=$(helm-deployer list -n $HELM_KUBERNETES_NAMESPACE -q -f $APPNAME-$CI_COMMIT_REF_SLUG | wc -l)

        echo "Chart instalado $CHART_INSTALED"

        if [ ${CHART_INSTALED} -eq 0 ]
        then
            echo 'First install'
            helm-deployer install $APPNAME-$CI_COMMIT_REF_SLUG --set labels.app=$APPNAME --set labels.version=$CI_COMMIT_REF_SLUG -f "/tmp/generated/configs/${ENV}.yaml" --namespace $HELM_KUBERNETES_NAMESPACE $HELM_CHART --atomic --wait
            exit 0
        fi

        helm-deployer upgrade --history-max 0 $APPNAME-$CI_COMMIT_REF_SLUG --set labels.app=$APPNAME --set labels.version=$CI_COMMIT_REF_SLUG -f "/tmp/generated/configs/${ENV}.yaml" --namespace $HELM_KUBERNETES_NAMESPACE $HELM_CHART --atomic --wait

.stop-kubernetes: &stop-kubernetes-template
    image: $HELM_DEPLOY_IMAGE
    when: manual
    allow_failure: true
    except:
        refs:
            - schedules
        variables:
            - $KUBERNETES_ENABLED == "false"
    before_script:  
        - *configure_kubectl
    script:
        - helm-deployer uninstall ${APPNAME}-${CI_COMMIT_REF_SLUG} --namespace $HELM_KUBERNETES_NAMESPACE

.stop-old-replicas-kubernetes: &stop-old-replicas-kubernetes-template
    image: $HELM_DEPLOY_IMAGE
    when: manual
    allow_failure: true
    except:
        refs:
            - schedules
        variables:
            - $KUBERNETES_ENABLED == "false"
    before_script:  
        - *configure_kubectl
    script: |
        bash
        if [ -z "$APPNAME" ]; then
            echo "A variavel APPNAME nao foi definida"
        	exit 1
        fi
        
        for deploy in $(helm ls -n $HELM_KUBERNETES_NAMESPACE -q -f $APPNAME | grep -v ${APPNAME}-${CI_COMMIT_REF_SLUG} | awk {'print $1'}); do
        
            helm-deployer uninstall ${deploy} --namespace $HELM_KUBERNETES_NAMESPACE

        done;

.promote-to-stable-kubernetes: &promote-to-stable-kubernetes-template
    image: $HELM_DEPLOY_IMAGE
    when: manual
    allow_failure: true
    except:
        refs:
            - schedules
        variables:
            - $KUBERNETES_ENABLED == "false"
    before_script:  
        - *configure_kubectl
    script: |
        echo "$HELM_KUBERNETES_NAMESPACE"
        echo "Substituindo variáveis"
        mkdir -p /tmp/generated/configs
        envsubst <${CI_PROJECT_DIR}/configs/routes.yaml > /tmp/generated/configs/routes.yaml
        echo "Promovendo rota"
        cat /tmp/generated/configs/routes.yaml
        kubectl apply -f /tmp/generated/configs/routes.yaml

# -----------------------------------
#  JOBS KUBERNETES TEST
# -----------------------------------

deploy-kubernetes-test:
    <<: *deploy-kubernetes-template
    extends:
        - .default-refs
    stage: test
    variables:
        ENV: 'test'
        KUBERNETES_DOMAIN: $KUBERNETES_DOMAIN_TEST
        HELM_KUBERNETES_NAMESPACE: $HELM_KUBERNETES_NAMESPACE_TEST
    only:
        variables:
            - $TEST_ENVIRONMENT_ENABLED == "true"
    environment:
        name: test/k8s/$CI_COMMIT_REF_NAME
        url: "https://${APPNAME}-${CI_COMMIT_REF_SLUG}.${KUBERNETES_DOMAIN_TEST}"
        on_stop: "stop-kubernetes-test"
    tags:
        - correspondente-express


stop-kubernetes-test:
    <<: *stop-kubernetes-template
    stage: test
    extends:
        - .default-refs
    variables:
        ENV: 'test'
        HELM_KUBERNETES_NAMESPACE: $HELM_KUBERNETES_NAMESPACE_TEST
        KUBERNETES_DOMAIN: $KUBERNETES_DOMAIN_TEST
    only:
        variables:
            - $TEST_ENVIRONMENT_ENABLED == "true"
    environment:
        name: test/k8s/$CI_COMMIT_REF_NAME
        action: stop
    tags:
        - correspondente-express

promote-to-stable-kubernetes-test:
    <<: *promote-to-stable-kubernetes-template
    extends:
        - .default-refs
    variables:
        DOMAIN: $KUBERNETES_DOMAIN_TEST
        URL: $KUBERNETES_URL_TEST
        HELM_KUBERNETES_NAMESPACE: $HELM_KUBERNETES_NAMESPACE_TEST
    stage: post test
    only:
        variables:
            - $TEST_ENVIRONMENT_ENABLED == "true"
    tags:
        - correspondente-express


# -----------------------------------
#  JOBS KUBERNETES PRODUCTION
# -----------------------------------

deploy-kubernetes-production:
    <<: *deploy-kubernetes-template
    stage: production
    extends:
        - .refs-production
    variables:
        ENV: 'production'
        KUBERNETES_DOMAIN: $KUBERNETES_DOMAIN_PRODUCTION
        HELM_KUBERNETES_NAMESPACE: $HELM_KUBERNETES_NAMESPACE_PRODUCTION
    only:
        variables:
            - $PRODUCTION_ENVIRONMENT_ENABLED == "true"
    environment:
        name: production/k8s/$CI_COMMIT_REF_NAME
        url: "https://${APPNAME}-${CI_COMMIT_REF_SLUG}.${KUBERNETES_DOMAIN_PRODUCTION}"
        on_stop: "stop-kubernetes-production"
    tags:
        - correspondente-express


stop-kubernetes-production:
    <<: *stop-kubernetes-template
    extends:
        - .refs-production
    stage: production
    variables:
        ENV: 'production'
        HELM_KUBERNETES_NAMESPACE: $HELM_KUBERNETES_NAMESPACE_PRODUCTION
        KUBERNETES_DOMAIN: $KUBERNETES_DOMAIN_PRODUCTION
    only:
        variables:
            - $PRODUCTION_ENVIRONMENT_ENABLED == "true"
    environment:
        name: production/k8s/$CI_COMMIT_REF_NAME
        action: stop
    tags:
        - correspondente-express


promote-to-stable-kubernetes-production:
    <<: *promote-to-stable-kubernetes-template
    extends:
        - .refs-production
    variables:
        DOMAIN: $KUBERNETES_DOMAIN_PRODUCTION
        URL: $KUBERNETES_URL_PRODUCTION
        HELM_KUBERNETES_NAMESPACE: $HELM_KUBERNETES_NAMESPACE_PRODUCTION
    stage: post production
    only:
        variables:
            - $PRODUCTION_ENVIRONMENT_ENABLED == "true"
    tags:
        - correspondente-express
